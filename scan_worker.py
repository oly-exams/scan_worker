#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Exam Tools
#
# Copyright (C) 2014 - 2018 Oly Exams Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division
from future import standard_library
standard_library.install_aliases()
from builtins import filter
from builtins import str
from builtins import range
import fitz
import struct
from PIL import Image
from io import BytesIO
import shutil
import os
import datetime
import subprocess
from tempfile import mkdtemp
import requests
import re
import contexttimer
import logging
import traceback
import random
import csv
logger = logging.getLogger('exam_tools.scan_worker')

temp_folder = mkdtemp(prefix="scan")

class ReScanException(Exception):
    pass

def all_same(items):
    return all(x == items[0] for x in items)

def is_valid_code(code):
    code_pattern = re.compile(r'^([^ ]+) ([^ ]+) ([^ ]+)$')
    match = code_pattern.match(code)
    if not match:
        logger.warning('Barcode "{}" is not valid. I will ignore it'.format(code))
        return False
    if match.group(1).count('-') not in (1,2):
        logger.warning('Number of `-` inconsistent in first group: '+code)
        return False
    if match.group(2).count('-') != 1:
        logger.warning('Number of `-` inconsistent in second group: '+code)
        return False
    if match.group(3).count('-') != 1:
        logger.warning('Number of `-` inconsistent in third group: '+code)
        return False
    return True

cropped_output_file = None
cropped_output_file_counter = 0

def crop_image(im, crop=None):
    global cropped_output_file_counter
    if crop is not None:
        width, height = im.size
        LEFT_CUT = 0.41 # previously: 0.35
        RIGHT_CUT = 0.6 # previously: 0.65
        HEIGHT_CUT = 0.16 # previously: 0.2
        UPPER_CUT = 0.03 # previously: 0.0
        if crop == 'upper':
            mask = (round(LEFT_CUT * width), round(height * UPPER_CUT), round(RIGHT_CUT * width), round(height * HEIGHT_CUT))
        else:
            mask = (round(LEFT_CUT * width), height - round(height / 5.), round(RIGHT_CUT * width), height - 1)
        im = im.crop(mask)
        # im.show()
    if cropped_output_file is not None:
        fn = os.path.splitext(cropped_output_file)[0] + '_{:03d}_{}'.format(cropped_output_file_counter, str(crop)) + '.png'
        im.save(fn)
        cropped_output_file_counter += 1
        cropped_output_file_counter %= 1000
    return im

def detect_barcode_zbarlight(img_path):
    import zbarlight

    im = Image.open(img_path)
    im.load()
    # im = im.convert(mode='I')

    for crop, rotate in [('upper', 0), ('lower', 180)]:
        for mode in ['RGB', '1', 'L', 'P', 'RGB', 'RGBA', 'CMYK', 'YCbCr', 'I', 'F']:
            for size_mult in [0.2, 0.5, 1.]:
                im_c = crop_image(im, crop)
                new_size = [round(s * size_mult) for s in im_c.size]
                im_c_convert = im_c.convert(mode=mode).resize(new_size)
                symbols = zbarlight.scan_codes(['qrcode'], im_c_convert)
                if symbols is None:
                    symbols = []
                symbols = [s.decode('ascii') for s in symbols]
                if len(symbols) > 0:
                    return symbols, rotate
    return symbols, rotate

def detect_barcode(img_path):
    symbols, rotate = detect_barcode_zbarlight(img_path)
    symbols = list(filter(is_valid_code, symbols))

    if len(symbols) == 0:
        return None, rotate
    elif not all_same(symbols):
        raise RuntimeError('Multiple barcodes detected and they are different! {}'.format(symbols))
    else:
        return symbols[0], rotate

def detect_barcodes_pdftoppm(input, ppi=150):
    """ Detect barcodes in a PDF file
    :param input: input file object
    :param ppi: resolution in points per inch for the PDF to image converion
    :return: List of tuples (page_number, detected_barcode)
    """
    pages = []
    logger.debug('starting pdftoppm')

    with contexttimer.Timer() as t:
        p = subprocess.Popen(
            # ["pdftoppm", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
            ["pdftoppm", "-gray", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
            # ["pdftoppm", "-gray", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()
    if len(os.listdir(temp_folder)) != 0:
        logger.debug("pdftoppm time per page: {0}s".format(t.elapsed / len(os.listdir(temp_folder))))
    err = p.stderr.read()
    logger.debug('pdftoppm done')
    if err:
        logger.error('PDFTOPPM PROCESSING ERROR: {}'.format(err))
    else:
        out = p.stdout.read()
        if out:
            logging.debug('pdftoppm processing log: {}'.format(out))
        logger.debug('starting to extract barcodes')
        for pg, fn in enumerate(sorted(os.listdir(temp_folder))):
            fp = os.path.join(temp_folder, fn)
            with contexttimer.Timer() as t:
                code, rotate = detect_barcode(fp)
            logger.debug("detect_barcode time per page: {0}s".format(t.elapsed))
            pages.append((pg, code, rotate))
            os.remove(fp)
        logger.debug('barcodes done')
    return pages


def get_timestamp():
    return datetime.datetime.now().strftime('%Y%m%d_%H%M%S')


def create_unique_filename(filename):
    if re.match('2\d{7}_\d{6}_\d{3}_', filename) is None:
        return "{}_{:03d}_{}".format(get_timestamp(), random.randint(0,999), filename)
    else:
        return filename


def page_sort(page_info):
    lookup_type = {"C" : 0, "A": 100, "W": 200, "Z": 300}  # do not use 900 or higher, 900 is used as default
    pg, code, rotate = page_info
    if code is None:
        return
    page_parts = code.split()[-1].split('-')
    try:
        val = lookup_type[page_parts[0]]
    except KeyError:
        logger.warning("page type '{}' not found, using default (900).".format(page_parts[0]))
        val = 900
    try:
        val += int(page_parts[1])
    except TypeError:
        logger.warning("failed to convert page number '{}' to int, using default (0).".format(page_parts[1]))
    return val


def process_basecode(url, key, pdfdoc, pages, msg, code, pgs, input, offline, expected_pages_csv, evaluated_dir, dryrun):
    if not offline:
        api_url = url + '/documents/'
        r = requests.get(api_url, allow_redirects=False, headers={
            'ApiKey': key
        }, params={
            'barcode_base': code,
        })
        r.raise_for_status()
        doc = r.json()[0]

        if doc['scan_status'] == 'S':
            raise ReScanException('There is already a successful scan for code {}. If you want to override it, change the status to W or M in the bulk print view and upload the scan again.'.format(code))


        expected_pages = doc['barcode_num_pages'] + doc['extra_num_pages']
    else:
        # expected_pages = len(pages) # to test and assume all pages have barcodes
        with open(expected_pages_csv) as csv_file:
            csv_reader = csv.reader(csv_file)
            expected_pages=[int(num) for cd, num in csv_reader if cd == code]
            if len(expected_pages) == 1:
                expected_pages = expected_pages[0]
            else:
                raise RuntimeError('Base code not or multiple times in given CSV: {}'.format(code))

    doc_complete = expected_pages == len(pgs)
    if not doc_complete:
        logger.warning('Missing pages: {} in DB but only {} in scanned document.'.format(expected_pages, len(pgs)))
        msg.append('Missing pages: {} in DB but only {} in scanned document.'.format(expected_pages, len(pgs)))
    pages_with_code_in_pgs = [(i, code, rotate) for i, code, rotate in pages if code is not None and i in pgs]
    ordered_pages = [(i, rotate) for i, code, rotate in sorted(pages_with_code_in_pgs, key=page_sort)]
    output = fitz.open()
    
    for i, rotate in ordered_pages[:-1]:
        rotate = (rotate + pdfdoc[i].rotation)%360
        output.insert_pdf(pdfdoc, from_page=i, to_page=i, rotate=rotate, final=False)   # force creation of new pdf, .select() might not actually delete data of the pages removed
    rotate = (ordered_pages[-1][1] + pdfdoc[ordered_pages[-1][0]].rotation)%360
    output.insert_pdf(pdfdoc, from_page=ordered_pages[-1][0], to_page=ordered_pages[-1][0], rotate=rotate, final=True)
    
    output_pdf = output.tobytes(garbage=3, clean=True, deflate=True)
    data = {}
    if not doc_complete and len(msg) == 1:
        data['scan_status'] = 'M'
    elif len(msg) > 0:
        data['scan_status'] = 'W'
    else:
        data['scan_status'] = 'S'
    data['scan_msg'] = '\n'.join(msg)

    if not offline and not dryrun:
        api_url = url + '/documents/{id}/'.format(**doc)
        r = requests.patch(api_url, allow_redirects=False, headers={
            'ApiKey': key
        }, files={
            'scan_file': (input.name, output_pdf),
            'scan_file_orig': (input.name, open(input.name, 'rb')),
        }, data=data)
        r.raise_for_status()

        logger.info('Scan document inserted in DB for barcode {}'.format(code))
    elif not dryrun:
        oname = code+'_'+get_timestamp()
        oname = os.path.join(evaluated_dir, oname)
        shutil.copy(input.name, oname+'_orig.pdf')
        with open(oname+'.pdf', 'wb') as f:
            f.write(output_pdf)

def main(input, url, key, scans_problems_dir, net_problems_dir, offline, expected_pages_csv, evaluated_dir, dryrun=False):
    """ Recognize QR codes in a given PDF file, check them for completeness and extract the respective pages
    into a new PDF file.
    Args:
        input: input file object
        url: URL of the API to witch to send the PDFs and extracted information to
        key: API key
        scans_problems_dir: directory in which to place PDFs and status files if failures during QR code recognization and checking occur
        net_problems_dir: directory in which to place PDFs and status files if failures durin upload occur
        dryrun: if True, Skip writing to DB and filesystem
    Returns:
        None
    """
    try:
        pages = detect_barcodes_pdftoppm(input)
        logger.info('got {} pages.'.format(len(pages)))
        logger.info('Barcodes: {}'.format([code for i,code,rotate in pages]))

        base_code_pattern = re.compile(r'(([^ ]+) ([^ ]+))')
        def get_base(code):
            match = base_code_pattern.match(code)
            return match.group(1) if match else None

        student_code_pattern = re.compile(r'([^ ]+)')
        def get_student(code):
            match = student_code_pattern.match(code)
            return match.group(1) if match else None

        basecodes = {get_base(code): [] for i, code, rotate in pages if code is not None}
        for i,code,rotate in pages:
            if code is not None:
                basecodes[get_base(code)].append(i)

        studentcodes = {get_student(basecode): [] for basecode in basecodes.keys()}

        msg_global = []
        if len(studentcodes) > 1:
            logger.warning('Pages belonging to different students detected. {}'.format(list(studentcodes.keys())))
            msg_global.append('Pages belonging to different students detected. {}'.format(list(studentcodes.keys())))
        pdfdoc = fitz.open(input.name)
        for code, pgs in list(basecodes.items()):
            msg = msg_global.copy()
            logger.debug('Processing: {}'.format(code))
            try:
                process_basecode(url, key, pdfdoc, pages, msg, code, pgs, input, offline, expected_pages_csv, evaluated_dir, dryrun)
            except requests.HTTPError as error:
                if not dryrun:
                    oname = code+'_'+get_timestamp()+'.pdf'
                    oname = os.path.join(net_problems_dir, oname)
                    shutil.copy(input.name, oname)
                    with open(oname+'.status', 'w') as f:
                        f.write('Barcode: {}\nHttp response:\n{}\n'.format(code, error.response.text.encode('utf-8')))
                logger.warning('Errors with code {}.\n{}'.format(code, error.response.text.encode('utf-8')))
            except ReScanException as error:
                if not dryrun:
                    oname = code+'_'+get_timestamp()+'.pdf'
                    oname = os.path.join(scans_problems_dir, oname)
                    shutil.copy(input.name, oname)
                    with open(oname+'.status', 'w') as f:
                        f.write('ReScanException:\n'+error.args[0])
                logger.warning('ReScanException:\n'+error.args[0])

        if len(basecodes) == 0:
            logger.warning('NO BARCODE DETECTED')
            raise RuntimeError('NO-BARCODE')

    except Exception as error:
        logger.error('An exception occured!\n'+str(error))
        if not dryrun:
            oname = os.path.basename(input.name)
            oname = os.path.join(scans_problems_dir, oname)
            shutil.copy(input.name, oname)
            with open(oname+'.status', 'w') as f:
                f.write('Exception:\n'+traceback.format_exc())
            logger.warning(u'Document has been saved to '+str(oname))


if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser(description='Import scan document to DB')
    parser.add_argument('--evaluating-dir', dest='evaluating_dir', type=str, default='scans-evaluating', help='Directory for storing documents that are being processed')
    parser.add_argument('--received-dir', dest='received_dir', type=str, default='scans-received', help='Directory for storing scans that were received by the script')
    parser.add_argument('--scans-problems-dir', dest='scans_problems_dir', type=str, default='scans-problems', help='Directory for storing problematic scans')
    parser.add_argument('--net-problems-dir', dest='net_problems_dir', type=str, default='scans-network-problems', help='Directory for storing problematic scans')
    parser.add_argument('--evaluated-dir', dest='evaluated_dir', type=str, default='scans-evaluated', help='Directory for storing evaluated scans in offline mode')
    parser.add_argument('--url', type=str, required='--offline' not in sys.argv, help='Url of the documents API, e.g. https://demo-apho.oly-exams.org/api/exam')
    parser.add_argument('--key', type=str, required='--offline' not in sys.argv, help='API Key')
    parser.add_argument('--dry-run', dest='dryrun', action='store_true', help='Skip writing to DB and filesystem')
    parser.add_argument('--offline', dest='offline', action='store_true', help='For offline testing: query csv file instead of DB and write results locally')
    parser.add_argument('--expected-pages', dest='expected_pages', required='--offline' in sys.argv, help='For offline testing: csv file with number of expected pages for each base code')
    parser.add_argument('--move-file', dest='move_file', action='store_true', help='Move incoming files away')
    parser.add_argument('--store-file', dest='store_file', action='store_true', help='Store a copy of successfully processed files')
    parser.add_argument('filename', help='Input PDF')
    parser.add_argument('-vv', '--more-verbose', help="Be more verbose", action="store_const", dest="loglevel", const=logging.DEBUG, default=logging.WARNING)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const", dest="loglevel", const=logging.INFO)
    parser.add_argument('--cropped-output-file', dest='cropped_output_file', type=str, help='Base filename for outputting a PNG of the last cropped image parts')
    args = parser.parse_args()

    cropped_output_file = args.cropped_output_file

    ch = logging.StreamHandler()
    formatter = logging.Formatter('[%(asctime)s - %(name)s] - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.setLevel(args.loglevel)

    if not os.path.isdir(args.evaluating_dir) and not args.dryrun:
        os.makedirs(args.evaluating_dir)
    if not os.path.isdir(args.received_dir) and not args.dryrun:
        os.makedirs(args.received_dir)
    if not os.path.isdir(args.scans_problems_dir) and not args.dryrun:
        os.makedirs(args.scans_problems_dir)
    if not os.path.isdir(args.net_problems_dir) and not args.dryrun:
        os.makedirs(args.net_problems_dir)
    if not os.path.isdir(args.evaluated_dir) and not args.dryrun:
        os.makedirs(args.evaluated_dir)

    logger.info('Process file {}'.format(args.filename))
    received_filename = os.path.join(args.received_dir, create_unique_filename(os.path.basename(args.filename)))
    if args.move_file:
        os.rename(args.filename, received_filename)
    else:
        shutil.copy(args.filename, received_filename)
    input_filename = os.path.join(args.evaluating_dir, os.path.basename(received_filename))
    shutil.copy(received_filename, input_filename)
    with open(input_filename, 'rb') as input:
        main(input, args.url, args.key, args.scans_problems_dir, args.net_problems_dir, args.offline, args.expected_pages, args.evaluated_dir, args.dryrun)
    os.remove(input_filename)
    if not args.store_file:
        os.remove(received_filename)
