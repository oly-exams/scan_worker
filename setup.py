#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from setuptools import setup, find_packages

import sys

readme = """Process PDF documents looking for Exam Tools barcodes"""
version = '0.6.1'

setup(
    name='scan-worker',
    py_modules=['scan_worker'],
    version=version,
    url='http://oly-exams.org',
    author='Oly Exams Team',
    author_email='michele.dolfi@oly-exams.org',
    description=readme,
    install_requires=['future', 'requests', 'zbarlight', 'pymupdf', 'contexttimer'],
    scripts=['scan_worker.py'],
    license='GNU Affero General Public License',
)
