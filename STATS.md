300 dpi, zbar
# Report for directory ../training_media/scans-problems/:
Total files: 4
Total pages: 46
Time per page: 0.759s
Pages with valid codes detected: 36 (78.3%)
Files with errors: 0 (0.0%)

150 dpi, zbar
# Report for directory ../training_media/scans-problems/:
Total files: 4
Total pages: 46
Time per page: 0.275s
Pages with valid codes detected: 32 (69.6%)
Files with errors: 0 (0.0%)


300 dpi, zbarlight
# Report for directory ../training_media/scans-problems/:
Total files: 4
Total pages: 46
Time per page: 1.389s
Pages with valid codes detected: 36 (78.3%)
Files with errors: 0 (0.0%)

150 dpi, zbarlight
# Report for directory ../training_media/scans-problems/:
Total files: 4
Total pages: 46
Time per page: 0.274s
Pages with valid codes detected: 32 (69.6%)
Files with errors: 0 (0.0%)


300 dpi, zxinglight
# Report for directory ../training_media/scans-problems/:
Total files: 4
Total pages: 46
Time per page: 1.496s
Pages with valid codes detected: 36 (78.3%)
Files with errors: 0 (0.0%)

150 dpi, zxinglight
# Report for directory ../training_media/scans-problems/:
Total files: 4
Total pages: 46
Time per page: 0.313s
Pages with valid codes detected: 36 (78.3%)
Files with errors: 0 (0.0%)


300 dpi, zxinglight
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 1.503s
Pages with valid codes detected: 145 (50.3%)
Files with errors: 0 (0.0%)

150 dpi, zxinglight
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.303s
Pages with valid codes detected: 77 (26.7%)
Files with errors: 0 (0.0%)


300 dpi, zbar
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 1.405s
Pages with valid codes detected: 176 (61.1%)
Files with errors: 0 (0.0%)

150 dpi, zbar
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.307s
Pages with valid codes detected: 164 (56.9%)
Files with errors: 0 (0.0%)


300 dpi, zbarlight
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 1.446s
Pages with valid codes detected: 176 (61.1%)
Files with errors: 0 (0.0%)


300 dpi, zxinglight, hybrid=True
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 1.827s
Pages with valid codes detected: 175 (60.8%)
Files with errors: 0 (0.0%)

150 dpi, zxinglight, hybrid=True
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.328s
Pages with valid codes detected: 164 (56.9%)
Files with errors: 0 (0.0%)

300 dpi, zbar, ["pdftoppm", "-gray", "-freetype", "no", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.591s
Pages with valid codes detected: 176 (61.1%)
Files with errors: 0 (0.0%)

300 dpi, zbar, ["pdftoppm", "-gray", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.642s
Pages with valid codes detected: 176 (61.1%)
Files with errors: 0 (0.0%)


300 dpi, zbar, ["pdftoppm", "-gray", "-freetype", "no", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
with lower, upper crop
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.364s
Pages with valid codes detected: 176 (61.1%)
Files with errors: 0 (0.0%)


150 dpi, zbar
["pdftoppm", "-gray", "-freetype", "no", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
with lower, upper crop
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.206s
Pages with valid codes detected: 178 (61.8%)
Files with errors: 0 (0.0%)


150 dpi, zxinglight, hybrid=False
["pdftoppm", "-gray", "-freetype", "no", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
with lower, upper crop
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.205s
Pages with valid codes detected: 124 (43.1%)
Files with errors: 0 (0.0%)


150 dpi, zxinglight, hybrid=True
["pdftoppm", "-gray", "-freetype", "no", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
with lower, upper crop
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.215s
Pages with valid codes detected: 172 (59.7%)
Files with errors: 0 (0.0%)


150 dpi, zbar, ["pdftoppm", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
with lower, upper crop
# Report for directory ../ipho_media/scans-test-sample/:
Total files: 10
Total pages: 288
Time per page: 0.248s
Pages with valid codes detected: 178 (61.8%)
Files with errors: 0 (0.0%)


150 dpi, zbar
["pdftoppm", "-gray", "-freetype", "no", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
with lower, upper crop
# Report for directory ../ipho_media/scans-test-sample2:
Total files: 29
Total pages: 461
Time per page: 0.209s
Pages with valid codes detected: 387 (83.9%)
Files with errors: 0 (0.0%)


300 dpi, zbar
["pdftoppm", "-gray", "-freetype", "no", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
with lower, upper crop
# Report for directory ../ipho_media/scans-test-sample2:
Total files: 29
Total pages: 461
Time per page: 0.357s
Pages with valid codes detected: 386 (83.7%)
Files with errors: 0 (0.0%)

150 dpi, zbar, cropping
["pdftoppm", "-gray", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
# Report for directory ../ipho_media/scans-evaluated/:
Total files: 2185
Total pages: 43160
Time per page: 0.389s
Pages with valid codes detected: 31100 (72.1%)
Files with errors: 1 (0.0%) [note this was a 0 kb PDF -> i.e. invalid]

300 dpi, zbar, no cropping
["pdftoppm", "-gray", "-aa", "no", "-aaVector", "no", "-r",  str(int(ppi)), input.name, os.path.join(temp_folder, "temp")],
# Report for directory ../ipho_media/scans-evaluated/:
Total files: 2184
Total pages: 43160
Time per page: 0.938s
Pages with valid codes detected: 30974 (71.8%)
Files with errors: 0 (0.0%)