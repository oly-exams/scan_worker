#!/bin/bash

python3 scan_worker.py --store-file --evaluating-dir="./test/scans-evaluating" --received-dir="./test/scans-received" --scans-problems-dir="./test/scans-problems" --net-problems-dir="./test/scans-network-problems" --evaluated-dir="./test/scans-evaluated" --offline --expected-pages="./multi_question_expected_pages.csv" -vv test_multi_question.pdf 2>&1
