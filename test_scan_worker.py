#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Exam Tools
#
# Copyright (C) 2014 - 2018 Oly Exams Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from builtins import filter
import os.path
import glob
import shutil
import contexttimer
import traceback

import scan_worker


def is_good_code(code):
    return code[1] is not None


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Test the scan-worker')
    parser.add_argument('directory', help='directory containing test PDFs', action='store')
    parser.add_argument('--cropped-output-file', dest='cropped_output_file', type=str,
                        help='Base filename for outputting a PNG of the last cropped image parts')

    args = parser.parse_args()
    scan_worker.cropped_output_file = args.cropped_output_file

    errors = 0
    pages = 0
    good_codes = 0

    pdfs = glob.glob(os.path.join(args.directory, '**.pdf'))
    with contexttimer.Timer() as t:
        for pdf in pdfs:
            print("> {0}".format(pdf))
            with open(pdf, 'rb') as f:
                try:
                    codes = scan_worker.detect_barcodes_pdftoppm(f)
                except Exception as err:
                    errors += 1
                    traceback.print_exc()
                    continue
                print(codes)
                pages += len(codes)
                good_codes += len(list(filter(is_good_code, codes)))

    print("# Report for directory {0}:".format(args.directory))
    print("Total files: {0}".format(len(pdfs)))
    print("Total pages: {0}".format(pages))
    if pages != 0:
        print("Time per page: {0:.3f}s".format(t.elapsed / pages))
        print("Pages with valid codes detected: {0} ({1:.1f}%)".format(good_codes, good_codes/float(pages)*100))
    if len(pdfs) != 0:
        print("Files with errors: {0} ({1:.1f}%)".format(errors, errors/float(len(pdfs))*100))
