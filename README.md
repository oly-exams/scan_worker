# Exam Tools - Scan worker

Process PDF documents looking for Exam Tools barcodes

* Process scanned documents as well as programmatic PDF
* Process multi page documents
* Extraction of pages with barcodes
* Sorting of pages according to barcode
* Send documents to Exam Tools server (API calls)


## Example deployment

As post-upload hook of an FTP server, e.g. [Pure-FTPd](https://www.pureftpd.org/).

TODO: example config


## Configuration

You need to provide the secret key from your Exam Tools installation.


## Command line options

TODO


## Testing

Simply provide the ```--dry-run``` flag to avoid saving result to the Exam Tools
instance and file system

### Test PDFs
- IPhO2016: https://drive.google.com/drive/folders/16GgvrOYdFXF1MnMyRJANNOxivbraXQtL?usp=share_link
- IPhO2019: https://drive.google.com/drive/folders/1cfOKOr2t1_OETPnlptor2xYx28GL_Tqz?usp=share_link

### Test Results
- IPhO2016-exams-docs: All QR codes detected besides 1 on a page with a blue bar through the code and 2 PDFs that could not be opened.
- IPhO2016-scans-evaluated: All QR codes detected besides 10 on pages where the code is not 100% visible and 1 PDF that could not be opened.
- IPhO2019-scans-evaluated: All QR codes detected.
- IPhO2019-scans-problems: Lots of PDFs that can not be opened, non conformal codes appear (e.g. T1 instead of T-1), no meaningful testing doable.