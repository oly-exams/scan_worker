#!/bin/bash

for f in ./ipho2019/*.pdf; do
    python3 scan_worker.py --evaluating-dir="./test/scans-evaluating" --received-dir="./test/scans-received" --scans-problems-dir="./test/scans-problems" --net-problems-dir="./test/scans-network-problems" --evaluated-dir="./test/scans-evaluated" --offline --expected-pages="./ipho2019/expected_pages.csv" -v "$f" 2>&1
done
